const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter
router.post('/', createFighterValid, (req, res, next) => {
    try {
        let name = req.body.name;
        let power = req.body.power;
        let defense=req.body.defense;
        let fighter = {name: name, power: power, defense: defense, health:100} ;
        FighterService.createFighter(fighter);
        res.status(200).send(JSON.stringify(fighter));
    } catch (err) {
        res.status(400).json({
        status: 400,
        message: `${err}`
    });
    } finally {
        next();
    }
  });
  
  router.get('/',(req, res, next)=>{
    try {
    let fighters=FighterService.getAll();
    res.status(200).send(JSON.stringify(fighters));
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: `${err}`
    });
    }
    finally {
      next();
    }
  }) 
  router.get('/:id',(req, res, next)=>{
    try {
    let id = req.params.id;
    let fighter=FighterService.search({id});
    res.status(200).send(JSON.stringify(fighter));
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: `${err}`
    });
    }
    finally {
      next();
    }
  }) 
  router.put('/:id', updateFighterValid, (req, res, next)=>{
    try {
        let id = req.params.id;
        let name = req.body.name;
        let power = req.body.power;
        let defense=req.body.defense;
        let newFighter = {name: name, power: power, defense: defense, health: 100};
        FighterService.updateFighter(id, newFighter);
        res.status(200).json({
        status: 200,
        message: "Fighter updated"
    });
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: `${err}`
    });
    }
    finally {
      next();
    }
  });
  router.delete('/:id',(req, res, next)=>{
    try {
      let id = req.params.id;
      FighterService.deleteFighter(id);
      res.status(200).json({
        status: 200,
        message: "Fighter deleted"
    });
    } catch (err) {
      res.status(400).json({
        status: 400,
        message: `${err}`
    });
    }
    finally {
      next();
    }
  })
module.exports = router;