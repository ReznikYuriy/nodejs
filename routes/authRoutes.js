const { Router } = require('express');
const AuthService = require('../services/authService');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', responseMiddleware, (req, res, next) => {
    try {
        // TODO: Implement login action (get the user if it exist with entered credentials)
        let {email, password}=req.body;
        let user=AuthService.login({email, password});
        res.status(200).send(JSON.stringify(user));
        //res.data = data;
    } catch (err) {
        res.status(400).json({
            status: 400,
            message: `${err}`,
            error: `${err}`
        });
    } finally {
        next();
    }
});

module.exports = router;