const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

router.post('/', createUserValid, (req, res, next) => {
  try {
      //console.log("post/api/users");
      let firstName = req.body.firstName;
      let lastName = req.body.lastName;
      let email=req.body.email;
      let phoneNumber=req.body.phoneNumber;
      let password=req.body.password;
      let user = {firstName: firstName, lastName: lastName, email:email, phoneNumber:phoneNumber, password:password} ;
      UserService.createUser(user);
      res.status(200).send(JSON.stringify(user));
  } catch (err) {
      res.status(400).json({
      status: 400,
      message: `${err}`,
      error: `${err}`
  });
  } finally {
      next();
  }
});

router.get('/',(req, res, next)=>{
  try {
  //console.log("get/api/users");
  let users=UserService.getAll();
  res.status(200).send(JSON.stringify(users));
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: `${err}`
  });
  }
  finally {
    next();
  }
}) 
router.get('/:id',(req, res, next)=>{
  try {
  let id = req.params.id;
  let user=UserService.search({id});
  res.status(200).send(JSON.stringify(user));
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: `${err}`
  });
  }
  finally {
    next();
  }
}) 
router.put('/:id', updateUserValid, (req, res, next)=>{
  try {
    let id = req.params.id;
    let firstName = req.body.firstName;
    let lastName = req.body.lastName;
    let email=req.body.email;
    let phoneNumber=req.body.phoneNumber;
    let password=req.body.password;
    let newUser = {firstName: firstName, lastName: lastName, email:email, phoneNumber:phoneNumber, password:password};
    UserService.updateUser(id, newUser);
    res.status(200).json({
      status: 200,
      message: "User updated"
  });
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: `${err}`
  });
  }
  finally {
    next();
  }
});
router.delete('/:id',(req, res, next)=>{
  try {
    let id = req.params.id;
    UserService.deleteUser(id);
    res.status(200).json({
      status: 200,
      message: "User deleted"
  });
  } catch (err) {
    res.status(400).json({
      status: 400,
      message: `${err}`
  });
  }
  finally {
    next();
  }
})


module.exports = router;