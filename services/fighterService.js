const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getAll(){
        const items = FighterRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    createFighter(data){
        const {name}=data;
        const _name=this.search({name});
        if(_name) {
            throw Error('Fighter with such name already added');
        }
        const item = FighterRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }
    search(search) {
        const item = FighterRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    updateFighter(id, dataToUpdate){
        FighterRepository.update(id, dataToUpdate);
    }
    deleteFighter(id){
        FighterRepository.delete(id);
    }
}

module.exports = new FighterService();