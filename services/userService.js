const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user
    getAll(){
        const items = UserRepository.getAll();
        if(!items) {
            return null;
        }
        return items;
    }

    createUser(data){
        const {phoneNumber, email}=data;
        const _phoneNumber=this.search({phoneNumber});
        if(_phoneNumber) {
            throw Error('User with such phone number already added');
        }
        const _email=this.search({email});
        if(_email) {
            throw Error('User with such email already added');
        }
        const item = UserRepository.create(data);
        if(!item) {
            return null;
        }
        return item;
    }
    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            return null;
        }
        return item;
    }
    updateUser(id, dataToUpdate){
        UserRepository.update(id, dataToUpdate);
    }
    deleteUser(id){
        UserRepository.delete(id);
    }
}

module.exports = new UserService();