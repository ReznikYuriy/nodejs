const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    if (!req.body.name||req.body.name==='') return res.status(400).json({
        status: 400,
        message: 'Error. Fighter`s name is empty. '
    });
    if (!req.body.power||(typeof req.body.power != 'number' && !isFinite(req.body.power))||req.body.power<1||req.body.power>100) return res.status(400).json({
        status: 400,
        message: 'Error. Power is not valid or empty. '
    }); 
    if (!req.body.defense||(typeof req.body.defense != 'number' && !isFinite(req.body.defense))||req.body.defense<1||req.body.defense>10) return res.status(400).json({
        status: 400,
        message: 'Error. Defense is not valid or empty. '
    });
    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    if (!req.body.name||req.body.name==='') return res.status(400).json({
        status: 400,
        message: 'Error. Fighter`s name is empty. '
    });
    if (!req.body.power||(typeof req.body.power != 'number' && !isFinite(req.body.power))||req.body.power<1||req.body.power>100) return res.status(400).json({
        status: 400,
        message: 'Error. Power is not valid or empty. '
    }); 
    if (!req.body.defense||(typeof req.body.defense != 'number' && !isFinite(req.body.defense))||req.body.defense<1||req.body.defense>10) return res.status(400).json({
        status: 400,
        message: 'Error. Defense is not valid or empty. '
    });
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;