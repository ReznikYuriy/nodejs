const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation

    if (!req.body.firstName||req.body.firstName==='') return res.status(400).json({
        status: 400,
        message: 'Error. First Name is empty. ',
        error: 'Error. First Name is empty. '
    });
    if (!req.body.lastName||req.body.lastName==="") return res.status(400).json({
        status: 400,
        message: 'Error. Last Name is empty.',
        error: 'Error. Last Name is empty.'
    });
    if (!req.body.password||req.body.password==="") return res.status(400).json({
        status: 400,
        message: 'Error. Password is empty. ',
        error: 'Error. Password is empty. '
    }); 
    if (String(req.body.password).length<3) return res.status(400).json({
        status: 400,
        message: 'Error. Password have to 3 or more characters. ',
        error: 'Error. Password have to 3 or more characters. '
    });
    if (!(/^\+380\d{9}$/i).test(req.body.phoneNumber)) return res.status(400).json({
        status: 400,
        message: 'Error. Phone number is not valid. ',
        error: 'Error. Phone number is not valid. '
    });
    if (!req.body.phoneNumber||req.body.phoneNumber==="") return res.status(400).json({
        status: 400,
        message: 'Error. Phone number is empty. ',
        error: 'Error. Phone number is empty. '
    }); 
    if (!req.body.email||req.body.email==="") return res.status(400).json({
        status: 400,
        message: 'Error. Email is empty. ', 
        error: 'Error. Email is empty. '
    }); 
    if (!(/^[\w.\-]{0,25}@(gmail)\.com$/i).test(req.body.email)) return res.status(400).json({
        status: 400,
        message: 'Error. Email is not valid. ',
        error: 'Error. Email is not valid. '
    });
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    if (!req.body.firstName||req.body.firstName==='') return res.status(400).json({
        status: 400,
        message: 'Error. First Name is empty. '
    });
    if (!req.body.lastName||req.body.lastName==="") return res.status(400).json({
        status: 400,
        message: 'Error. Last Name is empty. '
    });
    if (!req.body.password||req.body.password==="") return res.status(400).json({
        status: 400,
        message: 'Error. Password is empty. '
    }); 
    if (String(req.body.password).length<3) return res.status(400).json({
        status: 400,
        message: 'Error. Password have to 3 or more characters. '
    });
    if (!(/^\+380\d{9}$/i).test(req.body.phoneNumber)) return res.status(400).json({
        status: 400,
        message: 'Error. Phone number is not valid. '
    });
    if (!req.body.phoneNumber||req.body.phoneNumber==="") return res.status(400).json({
        status: 400,
        message: 'Error. Phone number is empty. '
    }); 
    if (!req.body.email||req.body.email==="") return res.status(400).json({
        status: 400,
        message: 'Error. Email is empty. '
    }); 
    if (!(/^[\w.\-]{0,25}@(gmail)\.com$/i).test(req.body.email)) return res.status(400).json({
        status: 400,
        message: 'Error. Email is not valid. '
    });
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;